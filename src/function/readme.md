# Function.
- Helper shutdown - Simple performance monitor and logger.

### Documentation.
#### Helper shutdown.
Add file to you project. That is all.
```php
require '/vendor/mogulkan/mogulTools/function/helper_shutdown.php';
```
  
---
##### Helper shutdown options.
Define log path for simply cvs logging.
```php
define('MOGULKAN_SHUTDOWN_LOG_PATH', '/tmp/');
```
Customize log name / log rotation time. String name for one big log file or date()'s format for log rotation.
```php
define('MOGULKAN_SHUTDOWN_LOG_NAME', 'Y-m-d');// It's default value for log name.
```
You can turn off logging for a script in runtime by set $mogulkanTimeLog to false. 
```php
global $mogulkanTimeLog = false;
```
You can turn on logging for certain scripts in runtime by changing standard behaviour. 
```php
require '/vendor/mogulkan/mogulTools/function/helper_shutdown.php';// Include. 
global $mogulkanTimeLog = false;// Turn off.
...

global $mogulkanTimeLog = true;// Turn on somewhere in script in different cicumstances.
```


