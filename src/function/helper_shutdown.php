<?php

namespace mogulkan\helper_shutdown;

/** Simple performance monitor and logger.
 *
 * Start logging by adding to end of index page
 * @notice it use a $_SERVER[ 'HTTP_HOST' ] parameter and $_SERVER[ 'REQUEST_URI' ] without sanitization, that may lead to security breach.
 * This is simply logger is not purposed for using in production environment.
 * Strongly recommended use a complicated logger.      
 *     
 * @example 
 * $mogulkanTimeLog = true;// Used for preventing logging by setting false during execution.
 * define('MOGULKAN_SHUTDOWN_LOG_PATH', '/mogulkan_pg_tmp/');// or define('MOGULKAN_SHUTDOWN_LOG_PATH', '/mogulkan_pg_tmp/mogulkan/helper_shutdown/');
 * define('MOGULKAN_SHUTDOWN_LOG_NAME', 'Y-m-d');// (Optional) Separate log's by logging time. It suited to date() function's argument format.
 *     
 * define('MOGULKAN_SHUTDOWN_LOG_TAG',  '');// (Optional) Short label for logging purpose
 * define('MOGULKAN_SHUTDOWN_LOG_VER',  '');// (Optional) Any version or any addition information.
 * define('MOGULKAN_SHUTDOWN_LOG_NOTE', '');// (Optional) Any note.
 */

$mogulkanTimeLog       =           true ;
$mogulkanTimeExecution = microtime(true);

register_shutdown_function(function()
{
    global      $mogulkanTimeExecution;
    if(false == $mogulkanTimeExecution){return null;}// Shutdown was disabled. 

    $microTime       = microtime(true);
    $timeFromInclude = $microTime - $mogulkanTimeExecution;
    if($timeFromInclude > 0.999)
    {
      $timeFromInclude = round($timeFromInclude, 2);
    }
    else
    {
      $timeFromInclude =   round($timeFromInclude  ,      6);
      $timeFromInclude = str_pad("$timeFromInclude", 8, '0');
    }

  // Execution time.
    echo "<mogulkan-helper-shutdown>";
    echo "<pre>Execution date: ".date('Y-m-d H:i:s').".</pre>";

        if(isset($_SERVER[ 'REQUEST_TIME_FLOAT' ])){$timeFromStart = $microTime - floatval($_SERVER[ 'REQUEST_TIME_FLOAT' ]);}
    elseif(isset($_SERVER[ 'REQUEST_TIME'       ])){$timeFromStart = $microTime - floatval($_SERVER[ 'REQUEST_TIME'       ]);}

    if(isset($timeFromStart))
    {
      if($timeFromStart > 0.999){$timeFromStart = round($timeFromStart, 2);}
      else                      {$timeFromStart = round($timeFromStart, 6);}

      $timeFromStartToInclude = str_pad(strval($timeFromStart - $timeFromInclude), 8, '0');
      $timeFromInclude        = str_pad(strval(                 $timeFromInclude), 8, '0');
      $timeFromStart          = str_pad(strval($timeFromStart                   ), 8, '0');
  
      $scriptExeTime = "<pre>Execution time total:          $timeFromStart seconds.</pre>";
      echo "<pre>Execution time before include: $timeFromStartToInclude seconds.</pre>";
    }

  // Execution duration.
    echo "<pre>Execution time after  include: $timeFromInclude seconds.</pre>";
    if(isset($scriptExeTime)){echo $scriptExeTime;}

  // Memory consummation time.
    echo '<pre>MemUsed:          '. str_pad(strval(round((memory_get_usage(    )/1024))), 5, '0') .' kb. MemPeak:           '. str_pad(strval(round((memory_get_peak_usage(    )/1024))), 5, '0') ." kb.</pre>";
    echo '<pre>MemUsed real size:'. str_pad(strval(round((memory_get_usage(true)/1024))), 5, '0') .' kb. MemPeak real size::'. str_pad(strval(round((memory_get_peak_usage(true)/1024))), 5, '0') ." kb.</pre>";
    echo '<style>';
    echo 'mogulkan-helper-shutdown{display:block;padding:8px;background-color:grey;}';
    echo  '</style>';
    echo "</mogulkan-helper-shutdown>";

  # Log to file.
    global $mogulkanTimeLog;
    if($mogulkanTimeLog && defined('MOGULKAN_SHUTDOWN_LOG_PATH'))
    {
      $requestTime    = intval(round($microTime));
      $fileNameSuffix = defined('MOGULKAN_SHUTDOWN_LOG_NAME') ? '_'.date(MOGULKAN_SHUTDOWN_LOG_NAME, $requestTime) : '';
      $fopen          = fopen(MOGULKAN_SHUTDOWN_LOG_PATH . $_SERVER[ 'HTTP_HOST' ].$fileNameSuffix.'.csv', 'a');
      if(is_resource($fopen))
      {
        if(!isset($timeFromStart         )){$timeFromStart          = 0;}
        if(!isset($timeFromStartToInclude)){$timeFromStartToInclude = 0;}
    
        $note = defined('MOGULKAN_SHUTDOWN_LOG_NOTE') ? str_replace(',', ' ', MOGULKAN_SHUTDOWN_LOG_NOTE) : '';
        $tag  = defined('MOGULKAN_SHUTDOWN_LOG_TAG' ) ? str_replace(',', ' ', MOGULKAN_SHUTDOWN_LOG_TAG ) : '';
        $ver  = defined('MOGULKAN_SHUTDOWN_LOG_VER' ) ? str_replace(',', ' ', MOGULKAN_SHUTDOWN_LOG_VER ) : '';
    
        $TPL_csv = PHP_VERSION.','. 
          date('z', $requestTime).','.date('W', $requestTime).','.
          date('Y', $requestTime).','.date('n', $requestTime).','.date('j', $requestTime).','.
          date('G', $requestTime).','.date('i', $requestTime).','.date('s', $requestTime).','.
          "$tag,$ver,$note,".
          "$timeFromStartToInclude,$timeFromInclude,$timeFromStart,".
          round((memory_get_usage(         )/1024)).','.
          round((memory_get_usage(     true)/1024)).','.
          round((memory_get_peak_usage(    )/1024)).','.
          round((memory_get_peak_usage(true)/1024)).','.
          $_SERVER[ 'HTTP_HOST' ].
          str_replace('/', ',', $_SERVER[ 'REQUEST_URI'  ]).','.
          str_replace('/', ',', $_SERVER[ 'DOCUMENT_URI' ])."\n";
        @fwrite($fopen, $TPL_csv);
        fclose($fopen);
      }
    }
  } 
);
/*
 * This script is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this script.  If not, see <https://www.gnu.org/licenses/>.
*/  
////////////////////////////////////////////////////////////////////////////////////////////////////
// Authors:     mogulkan                                                                          //
// Copyright:   mogulkan                                                                          //
// License:     GNU LGPLv3                                                                        //
// Created:     2017-08-02                                                                        //
// Edited:      2018-01-13                                                                        //
// Title:       Mogulkan helper shutdown                                                          //
// Type:        Helper function.                                                                  //
// Purpose      Give brief information about script execution.                                    //
// Description: Show script execution duration, and memory consummation.                          //
// Notice:                                                                                        //
//  #1 This function invalidate some/HTML document.                                               //
//  #2 This function don't count duration for loading document through network.                   //
//  #3 This function don't count duration for process document on client side.                    //
//  #4 Because pf #2 & #3, Grand total duration usually more that showed by given function.       //
////////////////////////////////////////////////////////////////////////////////////////////////////
